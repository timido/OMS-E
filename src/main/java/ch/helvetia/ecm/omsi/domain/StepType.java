package ch.helvetia.ecm.omsi.domain;

public enum StepType {
	ENTRY,
	PAPYRUS,
	POSY,
	DOCSUITE,
	VPS
}
