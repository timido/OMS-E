package ch.helvetia.ecm.omsi.domain;

import java.time.LocalDateTime;
import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;

import javax.persistence.*;

import lombok.*;

/**
 * Created by jt on 6/13/17.
 */
@Getter
@Setter
@EqualsAndHashCode(exclude = {"stepList"})
@Entity
public class PrintOrder {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	private UUID trackId;
	private String originator;
	@OneToMany(fetch = FetchType.EAGER, cascade = CascadeType.ALL, mappedBy = "auftrag")
	private List<Step> stepList;
	private OrderStatus status;
	LocalDateTime startedAt;
	LocalDateTime lastUpdateAt;

	public PrintOrder(UUID trackId, String originator, List<String> stepList) {
		final List<StepType> stepTypes = stepList.stream().map(s -> StepType.valueOf(s)).collect(Collectors.toList());
		this.trackId = trackId;
		this.originator = originator;
		final List<Step> steps = stepTypes.stream().map(stepType -> new Step(stepType)).collect(Collectors.toList());
		this.stepList = steps;
		this.status = OrderStatus.CREATED;
		this.startedAt = LocalDateTime.now();
	}

	public PrintOrder() {

	}
}
