package ch.helvetia.ecm.omsi.domain;

import javax.persistence.*;

import lombok.*;

/**
 * Created by jt on 6/13/17.
 */
@Getter
@Setter
@EqualsAndHashCode
@Entity
public class PrintData {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	@ManyToOne
	private Step step;
	private String locationXmlFile;
	private String locationData;

	public PrintData() {
	}

	public PrintData(String locationSideFile, String locationDaten) {
		this.locationXmlFile = locationSideFile;
		this.locationData = locationDaten;
	}
}
