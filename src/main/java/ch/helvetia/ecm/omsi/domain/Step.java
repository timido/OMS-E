package ch.helvetia.ecm.omsi.domain;

import java.time.LocalDateTime;

import javax.persistence.*;

import lombok.*;

@Getter
@Setter
@EqualsAndHashCode(exclude = {"auftrag"})
@Entity
public class Step {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	@ManyToOne
	private PrintOrder order;
	@OneToMany(fetch = FetchType.EAGER, cascade = CascadeType.ALL, mappedBy = "step")
	private PrintData data;
	private StepType stepType;
	private String locationXmlFile;
	private String locationData;
	private LocalDateTime started;
	private LocalDateTime ended;

	public Step(StepType stepType) {
		this.stepType = stepType;
	}
}
