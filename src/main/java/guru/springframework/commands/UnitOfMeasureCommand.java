package guru.springframework.commands;

import lombok.*;

/**
 * Created by jt on 6/21/17.
 */
@Getter
@Setter
@NoArgsConstructor
public class UnitOfMeasureCommand {
	private Long id;
	private String description;
}
