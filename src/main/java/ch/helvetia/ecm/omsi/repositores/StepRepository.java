package ch.helvetia.ecm.omsi.repositores;

import java.util.List;
import java.util.Optional;

import org.springframework.data.repository.CrudRepository;

import ch.helvetia.ecm.omsi.domain.*;

/**
 * @author Mike Fingerhut {@literal <mike@netmotion.ch>}
 */
public interface StepRepository extends CrudRepository<Step, Long> {
	Optional<Step> findByStepType(StepType stepType);

	List<Step> findAllByOrder(PrintOrder order);
}
