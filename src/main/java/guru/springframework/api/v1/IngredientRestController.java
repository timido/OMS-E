package guru.springframework.api.v1;

import java.util.Set;

import javax.annotation.Resource;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import guru.springframework.commands.IngredientCommand;
import guru.springframework.commands.RecipeCommand;
import guru.springframework.converters.IngredientCommandToIngredient;
import guru.springframework.converters.IngredientToIngredientCommand;
import guru.springframework.services.IngredientService;
import guru.springframework.services.RecipeService;

/**
 * Created by jt on 6/19/17.
 */
@RestController
@RequestMapping(IngredientRestController.BASE_URL)
public class IngredientRestController {

	public static final String BASE_URL = "/api/v1/recipes";
	private final RecipeService recipeService;

	@Resource
	IngredientService ingredientService;
	@Resource
	private IngredientToIngredientCommand ingredientToIngredientCommand;
	@Resource
	private IngredientCommandToIngredient ingredientCommandToIngredient;


	public IngredientRestController(RecipeService recipeService) {
		this.recipeService = recipeService;
	}

	@GetMapping("/{recipeId}/ingredients")
	public Set<IngredientCommand> listIngredients(@PathVariable String recipeId) {
		final RecipeCommand recipe = recipeService.findCommandById(Long.valueOf(recipeId));

		final Set<IngredientCommand> ingredients = recipe.getIngredients();
		return ingredients;
	}

	@GetMapping("/{recipeId}/ingredients/{ingridientId}")
	public IngredientCommand getIngredient(@PathVariable String recipeId, @PathVariable String ingridientId) {
		return ingredientService.findByRecipeIdAndIngredientId(Long.valueOf(recipeId), Long.valueOf(ingridientId));
	}

	@PostMapping("/{recipeId}/ingredients")
	@ResponseStatus(HttpStatus.CREATED)
	public IngredientCommand saveIngredient(@PathVariable String recipeId, @RequestBody IngredientCommand ingredientCommand) {
		ingredientCommand.setRecipeId(Long.valueOf(recipeId));
		return ingredientService.saveIngredientCommand(ingredientCommand);
	}
}
