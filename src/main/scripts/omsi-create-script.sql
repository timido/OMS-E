CREATE TABLE print_data (
  id                BIGINT NOT NULL AUTO_INCREMENT,
  step_id           BIGINT,
  location_xml_file VARCHAR(255),
  location_data     VARCHAR(255),
  PRIMARY KEY (id)
)
  ENGINE = InnoDB;

CREATE TABLE step (
  id                BIGINT NOT NULL AUTO_INCREMENT,
  order_id          BIGINT,
  step_type         VARCHAR(50),
  location_xml_file VARCHAR(255),
  location_data     VARCHAR(255),
  started           DATETIME,
  ended             DATETIME,
  PRIMARY KEY (id)
)
  ENGINE = InnoDB;

CREATE TABLE print_order (
  id           BIGINT NOT NULL AUTO_INCREMENT,
  track_id     VARCHAR(255),
  originator   VARCHAR(255),
  status       VARCHAR(50),
  started      DATETIME,
  last_updated DATETIME,
  PRIMARY KEY (id)
)
  ENGINE = InnoDB;

ALTER TABLE step
  ADD CONSTRAINT FKdbfsiv21ocsbt63sd6fg0ddje FOREIGN KEY (order_id) REFERENCES print_order (id);
ALTER TABLE print_data
  ADD CONSTRAINT FKdbfsiv21ocsbt63sd6fg0ddje FOREIGN KEY (step_id) REFERENCES step (id);

