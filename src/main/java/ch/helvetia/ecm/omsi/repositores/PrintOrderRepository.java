package ch.helvetia.ecm.omsi.repositores;

import java.util.Optional;
import java.util.UUID;

import org.springframework.data.repository.CrudRepository;

import ch.helvetia.ecm.omsi.domain.PrintOrder;

/**
 * @author Mike Fingerhut {@literal <mike@netmotion.ch>}
 */
public interface PrintOrderRepository extends CrudRepository<PrintOrder, Long> {
	Optional<PrintOrder> findByTrackId(UUID trackId);
}
