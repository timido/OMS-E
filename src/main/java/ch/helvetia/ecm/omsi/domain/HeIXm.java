package ch.helvetia.ecm.omsi.domain;

import java.time.LocalDateTime;
import java.util.List;
import java.util.UUID;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class HeIXm {
	public UUID uuid;
	public List<Step> currentStep;
	public StepType nextStep;
	public OrderStatus status;
	public String dataLocation;
	public String sideFileLocation;
	public LocalDateTime initalized;
	public LocalDateTime lastAction;

	public HeIXm() {
	}
}
