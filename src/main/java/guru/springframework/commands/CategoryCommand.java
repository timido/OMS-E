package guru.springframework.commands;

import lombok.*;

/**
 * Created by jt on 6/21/17.
 */
@Setter
@Getter
@NoArgsConstructor
public class CategoryCommand {
	private Long id;
	private String description;
}
