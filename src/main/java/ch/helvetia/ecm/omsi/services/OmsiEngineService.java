package ch.helvetia.ecm.omsi.services;

import java.util.List;
import java.util.UUID;

import ch.helvetia.ecm.omsi.domain.PrintOrder;

/**
 * @author Mike Fingerhut {@literal <mike@netmotion.ch>}
 */
public interface OmsiEngineService {
	PrintOrder createOrder(UUID trackId, String originator, List<String> stepList);

	void deleteById(Long idToDelete);
}
