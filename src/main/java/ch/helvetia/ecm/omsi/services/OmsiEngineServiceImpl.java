package ch.helvetia.ecm.omsi.services;

import java.util.List;
import java.util.UUID;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import ch.helvetia.ecm.omsi.domain.PrintOrder;
import ch.helvetia.ecm.omsi.repositores.PrintOrderRepository;
import lombok.extern.slf4j.Slf4j;

/**
 * Created by jt on 7/3/17.
 */
@Slf4j
@Service
public class OmsiEngineServiceImpl implements OmsiEngineService {


	private final PrintOrderRepository printOrderRepository;

	public OmsiEngineServiceImpl(PrintOrderRepository printOrderRepository) {

		this.printOrderRepository = printOrderRepository;
	}

	@Override
	@Transactional
	public PrintOrder createOrder(UUID trackId, String originator, List<String> stepList) {
		try {
			PrintOrder printOrder = printOrderRepository.findByTrackId(trackId).orElse(new PrintOrder(trackId, originator, stepList));
			return printOrderRepository.save(printOrder);
		} catch (Exception e) {
			//todo handle better
			log.error("Error occurred", e);
			e.printStackTrace();
		}
		return null;
	}

	@Override
	public void deleteById(Long idToDelete) {

	}
}
