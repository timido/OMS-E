package ch.helvetia.ecm.omsi.domain;

public enum OrderStatus {
	CREATED,
	RUNNING,
	PAUSED,
	CANCELED,
	SUCCEEDED
}
