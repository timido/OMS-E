# Spring Boot Recipe Application

[![CircleCI](https://circleci.com/gh/springframeworkguru/spring5-recipe-app.svg?style=svg)](https://circleci.com/gh/springframeworkguru/spring5-recipe-app)

This repository is for an example application based on a course built in my Spring Framework 5 - Beginner to Guru
I has been modified and extended to run on JBoss 7 WildFly and added RestFul Services to it.

Necessary changes were:

* ServletInitializer.java which is needed for JBoss to allow Ininitalize the RestServlet for the web container
* The pom.xml File needed to add the following dependency:

       <!-- *********** IMPORTANT ************************************
            Un comment the following dependency when running in JBoss
            When running as normal Spring Boot Application this needs to be
            commented out and preferably change on line 9
            <packaging>war</packaging>
            to
            <packaging>jar</packaging>
        
            ALRWAYS DO A MAVEN CLEAN INSTALL
            -->
    		<dependency>
    			<groupId>org.springframework.boot</groupId>
    			<artifactId>spring-boot-starter-tomcat</artifactId>
    			<scope>provided</scope>
    		</dependency>
    	<!-- *********** END IMPORTANT ********************************* -->
    		
* The packaging in the pom.xml needed to be changed from jar to war:

            <groupId>guru.springframework</groupId>
            <artifactId>oms-e</artifactId>
            <version>0.0.1-SNAPSHOT</version>
            <packaging>war</packaging>

Cheers Sparta1





