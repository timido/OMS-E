package guru.springframework.api.v1;

import java.util.Set;
import java.util.stream.Collectors;

import javax.annotation.Resource;

import org.springframework.web.bind.annotation.*;

import guru.springframework.commands.RecipeCommand;
import guru.springframework.converters.RecipeCommandToRecipe;
import guru.springframework.converters.RecipeToRecipeCommand;
import guru.springframework.domain.Recipe;
import guru.springframework.services.RecipeService;
import lombok.extern.slf4j.Slf4j;

/**
 * Created by jt on 6/19/17.
 */
@RestController
@Slf4j
@RequestMapping(RecipeRestController.BASE_URL)
public class RecipeRestController {

	public static final String BASE_URL = "/api/v1/recipes";
	private final RecipeService recipeService;
	@Resource
	private RecipeCommandToRecipe recipeCommandToRecipe;
	@Resource
	private RecipeToRecipeCommand recipeToRecipeCommand;

	public RecipeRestController(RecipeService recipeService) {
		this.recipeService = recipeService;
	}

	@GetMapping("/{id}")
	public RecipeCommand showById(@PathVariable String id) {

		final Recipe recipe = recipeService.findById(Long.valueOf(id));
		return recipeToRecipeCommand.convert(recipe);
	}

	@GetMapping
	public Set<RecipeCommand> showAll() {
		final Set<Recipe> recipes = recipeService.getRecipes();
		final Set<RecipeCommand> recipeCommands = recipes.stream().map(recipe -> recipeToRecipeCommand.convert(recipe)).collect(Collectors.toSet());
		return recipeCommands;
	}
}
